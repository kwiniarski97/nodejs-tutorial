const fs = require('fs')
const fileName = './1-json.json'

let currentData = JSON.parse(fs.readFileSync(fileName).toString())
currentData.age = 22;
currentData = { ...currentData, name: 'Karol' }
console.log(currentData);
fs.writeFileSync(fileName, JSON.stringify(currentData))
const buffer = fs.readFileSync(fileName);
console.log(buffer.toString())