const { addNote, getNotes, getNote, removeNote } = require('./notes')
const chalk = require('chalk')

const commands = [
    {
        command: 'add',
        describe: 'Add a new note',
        builder: {
            title: {
                describe: 'Title of a note',
                demandOption: true,
                type: 'string',
            },
            body: {
                describe: 'Content of a note',
                type: 'string'
            },
        },
        handler: ({ title, body }) => {
            addNote(title, body)
        },
    },
    {
        command: 'remove',
        describe: 'Remove a note',
        builder: {
            id: {
                describe: 'Id of note to delete',
                demandOption: true,
                type: 'number'
            }
        },
        handler: ({ id }) => {
            removeNote(id)
        }
    },
    {
        command: 'list',
        describe: 'List all notes',
        handler: () => {
            const notes = getNotes();
            notes.forEach(note => {
                printNote(note)
            })
        },
    },
    {
        command: 'read',
        describe: 'Read a note',
        builder: {
            id: {
                describe: 'Id of note to read',
                demandOption: true,
                type: 'number'
            }
        },
        handler: ({ id }) => {
            const note = getNote(id)
            if (note) {
               printNote(note)
            } else {
                console.log(`Note with id: ${id} - not found`)
            }
        },
    },
];


function printNote(note) {
    console.log(chalk.bold.green(note.title))
    console.log(note.body)
    console.log()
}

module.exports = commands;