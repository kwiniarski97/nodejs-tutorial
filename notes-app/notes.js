const fs = require('fs')
const notesFileName = 'notes.json';
const chalk = require('chalk')

function getNotes() {
    const fileContentBuffer = fs.readFileSync(notesFileName);
    return JSON.parse(fileContentBuffer.toString());
}

function addNote(title, body) {
    const notes = getNotes();
    const lastNote = notes.sort((n1, n2) => n2.id - n1.id)[0]
    const id = lastNote ? lastNote.id + 1 : 1
    notes.push({ id, title, body })
    writeNotes(notes)
}

function getNote(id) {
    const notes = getNotes()
    return notes.find(note => note.id == id)
}

function removeNote(id) {

    debugger;
    
    const notes = getNotes()
    const filteredNotes = notes.filter(note => note.id !== id)
    writeNotes(filteredNotes);
    if (notes.length > filteredNotes.length) {
        console.log(chalk.red('Note removed!'))
    } else {
        console.log(chalk.yellow('Note not found!'))
    }
}


function writeNotes(notes) {
    fs.writeFileSync(notesFileName, JSON.stringify(notes))
}

module.exports = { getNotes, addNote, getNote, removeNote }