const getNotes = require('./notes.js')
const fs = require('fs')
const yargs = require('yargs')
require('./commands.js').forEach(command => {
    yargs.command(command)
});

const notesBuffer = fs.readFileSync('notes.json')
if(!notesBuffer.toString()){
    fs.writeFileSync('notes.json', '[]');
}


yargs.parse()
