const request = require('request')

const geocode = (address, successCallback, errorCallback) => request({ uri: `https://api.mapbox.com/geocoding/v5/mapbox.places/${encodeURIComponent(address)}.json?access_token=pk.eyJ1Ijoia2Fyb2x3MTk5NyIsImEiOiJjazQxcDNndXIwMzluM2VrazN4aDd6ODV3In0.mtkK8-SO5DiLKaZjKXSOZw&limit=1`, json: true }, (error, response) => {
    if (error || response.body.message) {
        errorCallback(response.body.message)
    } else {
        successCallback(response.body.features[0].center)
    }
});

module.exports = geocode