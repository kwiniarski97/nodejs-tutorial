const geocode = require("./geocode");
const forecast = require("./forecast");
const argv = require("yargs").argv
// mapbox pk.eyJ1Ijoia2Fyb2x3MTk5NyIsImEiOiJjazQxcDNndXIwMzluM2VrazN4aDd6ODV3In0.mtkK8-SO5DiLKaZjKXSOZw

geocode(
  argv.location,
  geocodes => {
      console.log(argv.location, geocodes)
    forecast(
      geocodes[1],
      geocodes[0],
      result => {
        console.log(result);
      },
      error => {
        console.error(error);
      }
    );
  },
  error => {
    console.error(error);
  }
);
