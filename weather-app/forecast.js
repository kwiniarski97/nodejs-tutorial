const request = require("request");

const forecast = (lat, lng, successCallback, failureCallback) => {
  request(
    {
      url: `https://api.darksky.net/forecast/0485d96a2bb0f13edef0805f8f6314fc/${lat},${lng}`,
      json: true
    },
    (error, { body }) => {
      if (error) {
        failureCallback(error);
      } else {
        successCallback({ body });
      }
    }
  );
};

module.exports = forecast;
