const express = require("express");
const path = require('path')

const app = express();

const public = path.join(__dirname, '../public')

app.use(express.static(public))

app.get("/", (req, res) => {
  res.render('index.html')
});

app.get("/help", (req, res) => {
  res.send({pozdro: 'help'});
});

app.get("/about", (req, res) => {
  res.render('about.html')
});

app.get("/weather", (req, res) => {
  res.send("weather");
});

app.listen(3000);
